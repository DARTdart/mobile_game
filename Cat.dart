import 'Animal.dart';

class Cat extends Animal {

  late String name;
  late int x;
  late int y;
  late int blood;
  late int energy;

  Cat(super.name, super.x, super.y);

  void speak() {
    print("meow!!!");
  }

  void eat() {
    print("eat!!!");
  }
}