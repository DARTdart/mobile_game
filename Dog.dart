import 'Animal.dart';

class Dog extends Animal {
  late String name;
  late int x;
  late int y;

  Dog(super.name, super.x, super.y);

  void speak() {
    print("bark!!!");
  }

  void bite() {
    print("kill kitten TT");
  }

}